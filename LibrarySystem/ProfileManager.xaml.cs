﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LibrarySystem
{
    /// <summary>
    /// Interaction logic for Window2.xaml
    /// </summary>
    public partial class ProfileManager : Window
    {
        public ProfileManager()
        {
            InitializeComponent();
        }

        private void PromoteUser_Click(object sender, RoutedEventArgs e)
        {
            PromoteUser promote = new PromoteUser();
            promote.Show();
            this.Close();
        }

        private void DemoteUser_Click(object sender, RoutedEventArgs e)
        {
            DemoteUser demote = new DemoteUser();
            demote.Show();
            this.Close();
        }

        private void RegisterUser_Click(object sender, RoutedEventArgs e)
        {
            Register register = new Register();
            register.Show();
            this.Close();
        }

        private void HomeButton_Click(object sender, RoutedEventArgs e)
        {
            MainMenu main = new MainMenu();
            main.Show();
            this.Close();
        }
    }
}
