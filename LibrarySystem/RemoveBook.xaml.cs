﻿using LibrarySystem._src.Dictionary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LibrarySystem
{
    /// <summary>
    /// Interaction logic for RemoveBook.xaml
    /// </summary>
    public partial class RemoveBook : Window
    {
        public RemoveBook()
        {
            InitializeComponent();
        }

        private void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            int ISBN = int.Parse(ISBNtextBox.Text);
            Books books = new Books();
            bool result = books.removeBook(ISBN);
            if (result)
            {
                MessageBox.Show("Book sucessfuly removed");
            }
            else
            {
                MessageBox.Show("Book with the same ISBN doesnt exist");
            }
        }

        private void HomeButton_Click(object sender, RoutedEventArgs e)
        {
            MainMenu main = new MainMenu();
            main.Show();
            this.Close();
        }
    }
}
