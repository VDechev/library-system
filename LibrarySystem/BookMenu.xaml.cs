﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LibrarySystem
{
    /// <summary>
    /// Interaction logic for BookMenu.xaml
    /// </summary>
    public partial class BookMenu : Window
    {
        public BookMenu()
        {
            InitializeComponent();
        }

        private void SearchForBook_Click(object sender, RoutedEventArgs e)
        {
            SearchForBook search = new SearchForBook();
            search.Show();
            this.Close();
        }

        private void AddBook_Click(object sender, RoutedEventArgs e)
        {
            AddBook add = new AddBook();
            add.Show();
            this.Close();
        }

        private void RemoveBook_Click(object sender, RoutedEventArgs e)
        {
            RemoveBook remove = new RemoveBook();
            remove.Show();
            this.Close();
        }

        private void HomeButton_Click(object sender, RoutedEventArgs e)
        {
            MainMenu main = new MainMenu();
            main.Show();
            this.Close();
        }
    }
}
