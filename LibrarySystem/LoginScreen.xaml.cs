﻿using LibrarySystem._src.Dictionary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LibrarySystem
{
    /// <summary>
    /// Interaction logic for LoginScreen.xaml
    /// </summary>
    public partial class LoginScreen : Window
    {
        public LoginScreen()
        {
            InitializeComponent();
        }
        public static Account currAcc;
        private void TxtButton_Click(object sender, RoutedEventArgs e)
        {
      
            Accounts accounts = new Accounts();

            accounts.fillSome();
            if (accounts.canLogin(txtUsername.Text, txtPassword.Text.ToCharArray()))
            {
                currAcc = accounts.getCurrentAcc();
                MainMenu main = new MainMenu();
                main.Show();
                this.Close();
            }
            else {
                MessageBoxResult result = MessageBox.Show("Wrong username or password");

              

            }

        }

    }
}
