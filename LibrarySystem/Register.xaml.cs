﻿using LibrarySystem._src.Dictionary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LibrarySystem
{
    /// <summary>
    /// Interaction logic for Register.xaml
    /// </summary>
    public partial class Register : Window
    {
        public Register()
        {
            InitializeComponent();
        }

        private void RegisterButton_Click(object sender, RoutedEventArgs e)
        {
            Accounts promote = new Accounts(LoginScreen.currAcc);
            string username = usernameRegister.Text;
            char[] password = passwordRegister.Text.ToCharArray();
            bool val = Convert.ToBoolean(isAdminRegister.Text);
            bool result = promote.Register(username, password,val);
            if (result)
            {
                MessageBox.Show("User registered");
            }
            else
            {
                MessageBox.Show("You have to be admin to register users or username is already taken");
            }
        }
        private void HomeButton_Click(object sender, RoutedEventArgs e)
        {
            MainMenu main = new MainMenu();
            main.Show();
            this.Close();
        }

        private void HomeButton_Click_1(object sender, RoutedEventArgs e)
        {
            MainMenu main = new MainMenu();
            main.Show();
            this.Close();
        }
    }
}
