﻿using LibrarySystem._src.Books;
using LibrarySystem._src.Dictionary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LibrarySystem
{
    /// <summary>
    /// Interaction logic for SearchForBook.xaml
    /// </summary>
    public partial class SearchForBook : Window
    {
        public SearchForBook()
        {
            InitializeComponent();
        }
        private Book book;
        private void HomeButton_Click(object sender, RoutedEventArgs e)
        {
            MainMenu main = new MainMenu();
            main.Show();
            this.Close();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            int ISBN = int.Parse(ISBNTextBox.Text);
            Books books = new Books();
            book = books.searchForBook(ISBN);

            if (book != null) {
                titleTextBox.Content = book.name;
                authorsTextBox.Content = book.authors;
                DescriptionTextBox.Content = book.description;
                ReleaseDateTextBox.Content = book.releaseDate;
                IsRentedTextBox.Content = book.isRented;
                
            }
            else
            {
                MessageBox.Show("Book with this ISBN doenst exist");
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Books books = new Books();
            int ISBN = int.Parse(ISBNTextBox.Text);
          bool res=  books.markBookAsRented(ISBN);

            if (res) {
                MessageBox.Show("Book succesfuly marked as rented");
            }

        }
    }
}
