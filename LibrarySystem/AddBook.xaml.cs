﻿using LibrarySystem._src.Dictionary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LibrarySystem
{
    /// <summary>
    /// Interaction logic for AddBook.xaml
    /// </summary>
    public partial class AddBook : Window
    {
        public AddBook()
        {
            InitializeComponent();
        }

        private void HomeButton_Click(object sender, RoutedEventArgs e)
        {
            MainMenu main = new MainMenu();
            main.Show();
            this.Close();
        }

        private void AddBookButton_Click(object sender, RoutedEventArgs e)
        {
            string title = titleTextBox.Text;
            string authors = authorsTextBox.Text;
            string description = Description.Text;
            bool isRented = Convert.ToBoolean(IsRented.Text);
            DateTime releaseDate = DateTime.ParseExact(ReleaseDateTextBox.Text, "d-M-yyyy", null);
            int ISBN = int.Parse(ISBNTextBox.Text);
            Books books = new Books();
           bool result = books.addBook(ISBN, title, description, authors, releaseDate, isRented);
            if (result)
            {
                MessageBox.Show("Book sucessfuly added");
            }
            else {
                MessageBox.Show("Book with the same ISBN already exists");
            }
        }
    }
}
