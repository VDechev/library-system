﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibrarySystem._src.Books
{
   public class Book
    {
        public Book() { }
        public Book(int ISBN, string name, string description, string authors, DateTime releaseDate, bool isRented)
        {

            this.ISBN = ISBN;
            this.name = name;
            this.description = description;
            this.authors = authors;
            this.releaseDate = releaseDate;
            this.isRented = isRented;
        }

        public int ISBN { get; } 
        public string name { get; } 

        public string description { get; }

        public string authors { get; }

        public DateTime releaseDate { get; }

        public bool isRented { get; set; }



    }
}
