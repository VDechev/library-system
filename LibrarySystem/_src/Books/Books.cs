﻿using LibrarySystem._src.Books;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace LibrarySystem._src.Dictionary
{
    public class Books
    {
        public Books() {
           
            books = Deserialize();
        }
        private Dictionary<int, Book> books;
        public Book searchForBook(int ISBN) {
            if (!isThereSuckABook(books, ISBN))
            {
                return null ;
            }
            else {
                Book book = books[ISBN];
                return book;
            }
        }
        public bool addBook(int ISBN, string name, string description, string authors, DateTime releaseDate, bool isRented) {
            if (isThereSuckABook(books, ISBN)) {
                return false;
            }
            else {
                books.Add(ISBN, new Book(ISBN, name, description, authors, releaseDate, isRented));
                SerializeNow(books);
                    return true; }

        }
        public bool removeBook(int ISBN) {
            if (isThereSuckABook(books, ISBN)) {
                return false;
            }

            else
            {
                books.Remove(ISBN);
                SerializeNow(books);
                return true;
            }
        }
        public bool markBookAsRented(int ISBN) {
            if (!isThereSuckABook(books, ISBN)) {
                return false;
            }
            Book book = books[ISBN];
            if (book.isRented)
            {
                return false;
            }
            else {
                book.isRented = true;
                books[ISBN] = book;
                SerializeNow(books);
                return true;
            }
        }
        public void saveChanges() {
            SerializeNow(books);
        }
        private void SerializeNow(Dictionary<int, Book> myObject)
        {

            FileStream writer = new FileStream("Books.xml", FileMode.Create);
            DataContractSerializer ser =
                new DataContractSerializer(typeof(Dictionary<int, Book>));
            ser.WriteObject(writer, myObject);
            writer.Close();
        }

        private Dictionary<int, Book> Deserialize()
        {
            FileStream fs = new FileStream("Books.xml",
            FileMode.Open);
            XmlDictionaryReader reader =
                XmlDictionaryReader.CreateTextReader(fs, new XmlDictionaryReaderQuotas());
            DataContractSerializer ser = new DataContractSerializer(typeof(Dictionary<int, Book>));
            Dictionary<int, Book> accounts =
                (Dictionary<int, Book>)ser.ReadObject(reader, true);
            reader.Close();
            fs.Close();
            return accounts;
        }
        private bool isThereSuckABook(Dictionary<int,Book> books, int ISBN) {
            return books.ContainsKey(ISBN);
        }
    }

}
