﻿using Polenter.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Controls;
using System.Xml;
using System.Xml.Serialization;

namespace LibrarySystem._src.Dictionary
{
    public class Accounts
    {
        public Accounts() {
          
        }
        public Accounts(Account acc)
        {
            this.currentAccount = acc;
        }
        private Account currentAccount;
        private string currentUsername;
        
        private bool login(string username, char[] password) {
            Dictionary<string, Account> accounts = Deserialize();
            if (searchForAcc(username, accounts)) {
                Account acc = accounts[username];
                if (acc.checkIfPasswordIsCorrect(password))
                {
                    currentAccount = acc;
                    currentUsername = username;
                    return acc.checkIfPasswordIsCorrect(password);
                }
            }
            return false;
        }
        private bool searchForAcc(string username, Dictionary<string, Account> accounts) {

        
       
            return accounts.ContainsKey(username);
        }
        private void SerializeNow(Dictionary<string, Account> myObject)
        {

            FileStream writer = new FileStream("Accounts.xml", FileMode.Create);
            DataContractSerializer ser =
                new DataContractSerializer(typeof(Dictionary<string, Account>));
            ser.WriteObject(writer, myObject);
            writer.Close();
        }

        private Dictionary<string, Account> Deserialize() {
            FileStream fs = new FileStream("Accounts.xml",
            FileMode.Open);
            XmlDictionaryReader reader =
                XmlDictionaryReader.CreateTextReader(fs, new XmlDictionaryReaderQuotas());
            DataContractSerializer ser = new DataContractSerializer(typeof(Dictionary<string, Account>));
            Dictionary<string, Account> accounts =
                (Dictionary<string, Account>)ser.ReadObject(reader, true);
            reader.Close();
            fs.Close();
            return accounts;
        }
        public bool canLogin(string username, char[] password) {
            return login(username, password);
        }
        public void fillSome() {
            Dictionary<string, Account> accounts = new Dictionary<string, Account>();
            //char[] username = { 'A', 'd', 'm', 'i', 'n' };
            string username = "Admin";
            char[] pass = { '1', '2', '3', '4', '5' };
            Account account = new Account(username, pass, true);

            accounts.Add(username, account);

            SerializeNow(accounts);

        }

        public bool Register(string username,char[] password, bool isAdmin) {
            Dictionary<string,Account> usernames = Deserialize();
            if (!canRegister()) {
                return false;
            }
            if (isUsernameTaken(username,usernames)) {
                return false;
            }
            Account account = new Account(username, password, isAdmin);
            usernames.Add(username, account);
            SerializeNow(usernames);
           
            return true;

         
        }

        private bool canRegister() {
            return currentAccount.isAdministrator();
        }

        private bool isUsernameTaken(string username, Dictionary<string,Account> acc) {
            return acc.ContainsKey(username);
        }

        public bool promoteDemoteUser(string username, bool isAdmin) {
            Dictionary<string, Account> usernames = Deserialize();
         
            if (!canRegister()) {
                return false;
            }
            if (!isUsernameTaken(username,usernames)) {
                return false;
            }
            Account acc = usernames[username];
            acc.promoteDemote(isAdmin);
            usernames[username] = acc;
            SerializeNow(usernames);
            return true;
        
        }

        public bool changePassword(char[] oldPassword, char[] newPassword, char[] repeatNewPassword) {
            Dictionary<string, Account> usernames = Deserialize();
            currentAccount.changePassword(oldPassword, newPassword, repeatNewPassword);
            usernames[currentUsername] = currentAccount;
            SerializeNow(usernames);

            return true;
        }
        public Account getCurrentAcc() {
            return currentAccount;
        }
    }

  
}
