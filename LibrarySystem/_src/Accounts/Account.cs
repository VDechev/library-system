﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibrarySystem._src.Dictionary
{
    public class Account
    {
        public Account(string username, char[] password, Boolean isAdmin) {
            this.username = username;
            this.password = password;
            this.isAdmin = isAdmin;
        }
        public Account() {

        }

        public string username;
        public char[] password;
        public Boolean isAdmin;
        public Boolean isAdministrator() {
            return isAdmin;
        }
        public void promoteDemote(bool isAdmin) {
           this.isAdmin = isAdmin;
        }
        public Boolean changePassword(char[] oldPassword, char[] newPassword, char[] repeatNewPassword) {
            string s = new string(oldPassword);
            string t = new string(password);

            int c = string.Compare(s, t);

            if (c != 0)
            {
                return false;
            }
            s = new string(newPassword);
            t = new string(repeatNewPassword);
            c = string.Compare(s, t);

            if (c != 0)
            {
                return false;
            }

            password = newPassword;
            return true;
        }

        public bool checkIfPasswordIsCorrect(char[] passwordForLogin) {
            string s = new string(passwordForLogin);
            string t = new string(password);
            int c = string.Compare(s, t);

            if (c == 0)
            {
                return true;
            }
            return false;
        }

  
    }

    }
        
      

