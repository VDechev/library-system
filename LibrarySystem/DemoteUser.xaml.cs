﻿using LibrarySystem._src.Dictionary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LibrarySystem
{
    /// <summary>
    /// Interaction logic for DemoteUser.xaml
    /// </summary>
    public partial class DemoteUser : Window
    {
        public DemoteUser()
        {
            InitializeComponent();
        }

        private void DemoteButton_Click(object sender, RoutedEventArgs e)
        {
            Accounts promote = new Accounts(LoginScreen.currAcc);
            string username = usernameTextBox.Text;
            bool result = promote.promoteDemoteUser(username, false);
            if (result)
            {
                MessageBox.Show("User demoted");
            }
            else
            {
                MessageBox.Show("You have to be admin to demote users or username is incorrect");
            }
        }
      

        private void HomeButton_Click_1(object sender, RoutedEventArgs e)
        {
            MainMenu main = new MainMenu();
            main.Show();
            this.Close();
        }
    }
}
